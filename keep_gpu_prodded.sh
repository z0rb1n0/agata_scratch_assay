#!/bin/bash -eu

declare -r -x PATH='/usr/local/bin:/usr/bin:/bin';

declare -r -i MIN_LOAD=30;


declare -i renderer_pid=-1;
declare -i renderer_wid=-1;

declare -i gpu_load=-1;

	#renice --priority 19 --pid ${$};

	exec &>"${HOME}/keep_gpu_prodded.log";

	trap 'kill -INT ${renderer_pid} 2>/dev/null' EXIT;

	while true; do
		if [ ${renderer_pid} -le 0 ] || (! kill -0 ${renderer_pid} 2>'/dev/null'); then
			#nice -n 19 glxgears &>'/dev/null'&
			#glmark2 --size=1920x1080 -b refract:duration=1000000000.0 &>'/dev/null'&
			#glmark2 --size=1920x1080 -b refract:duration=1000000000.0 --off-screen &>'/dev/null'&
			glmark2 --size=1920x1080 -b refract --off-screen --run-forever &>'/dev/null'&
			renderer_pid=${!};
			sleep 1;
			renderer_wid=$(xdotool search --name 'glxrenderer' | tail -n 1);
			#xdotool windowsize ${renderer_wid} '100%' '100%';
			#xdotool windowunmap ${renderer_wid};
			kill -STOP ${renderer_pid};
			continue;
		fi;


		gpu_load=$(
			nvidia-smi -q --display=UTILIZATION | \
			grep -i -P '^\s*GPU\s*:' | \
			awk -F '\s*(:|%)\s*' '{printf("%s\n", $2);}' \
		);

		printf '%s: LOAD %02d%% - ' "$(date --utc +'%Y-%m-%d %H:%M:%S')" ${gpu_load} 1>&2;
		if [ ${gpu_load} -lt ${MIN_LOAD} ]; then
			printf 'PRODDING' 1>&2;
			kill -CONT ${renderer_pid};
			sleep 0.2;
			kill -STOP ${renderer_pid};
		else
			printf 'SLEEPING' 1>&2;
		fi;
		printf '\n'

		sleep 0.4;
	done;
