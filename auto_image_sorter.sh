#!/bin/bash -eu



declare -r -x PATH='/usr/local/bin:/usr/bin:/bin';


declare -r TARGET_DIR="${HOME}/Pictures/auto_sorted";


	set -eu;

	if [ ${#} -lt 1 ]; then
		printf 'Usage:\n' 1>&2;
		printf '\t%s <source_directory>\n' 1>&2;
		exit 1;
	fi;


	if [ ! -d "${TARGET_DIR}" ]; then
		printf 'Target directory `%s` does not exist. Please create it\n' "${TARGET_DIR}" 1>&2;
		exit 3;
	fi;

	printf 'This command will DELETE & REBUILD all contents of `%s`\n' "${TARGET_DIR}" 1>&2;
	read -r -p 'Proceed? (Type uppercase YES to confirm): ' confirmation;
	[ "${confirmation}" == 'YES' ] || exit 0;


	rm -rf "${TARGET_DIR}/"*;



	find "$(readlink --canonicalize "${1}")" -type f | grep -i -P '\.((JPG)|(PNG)|(BMP)|(TIFF?))$' | while read -r next_file; do

		next_ts="$(exiv2 -g 'Exif.Photo.DateTimeOriginal' -Pv pr "${next_file}" | sed -r 's/^([0-9]+)[:-]([0-9]+)[:-](.*)/\1-\2-\3/g')";
		
		if ! [[ "${next_ts}" =~ ^[0-9]+- ]]; then
			printf 'WARNING: File `%s` has no Exif.Photo.DateTimeOriginal tag and will be skipped\n' "${next_file}" 1>&2;
			continue;
		fi;
		
		ts_dir="${TARGET_DIR}/$(date --utc '+%Y/%m-%d' --date="${next_ts}")";

		mkdir -p "${ts_dir}";

		if [ -a "${ts_dir}/${next_file##*/}" ]; then
			printf 'WARNING: File with the same relative name as `%s` was already seen. Skipping\n' "${next_file}" 1>&2;
			continue;
		fi;

		ln -s "${next_file}" "${ts_dir}";

	done;



#(set -eu && the_file='./Pictures/tahiti_2015/P1140603.JPG' && date --date="$(exiv2 -K Exif.Photo.DateTimeOriginal -Pv pr "${the_file}" | sed -r 's/^([0-9]+)[:-]([0-9]+)[:-](.*)/\1-\2-\3/g')")

