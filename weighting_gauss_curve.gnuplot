set terminal x11 size 2560, 1440;
set samples 1024, 1024;

set datafile separator ":";
set xzeroaxis;
set yzeroaxis;

set xrange [-2:2];
set yrange [0:1];



set title sprintf("Weight curve for scratch assay cells\n");


e = 2.71828182845904523536

height = 1.0
mu = 0.5
sigma = 1.0 / (e * pi)

plot \
	exp((-(x - mu) ** 2.0)) \
;


