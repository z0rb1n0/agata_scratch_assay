#!/usr/bin/python3 -uB


import sys, os, time, scipy, re, numpy, cv2, struct, math

from scipy import ndimage
import matplotlib.pyplot as plt

DATA_PATH = os.environ["HOME"] + "/agata_lab/2018-06-19 fluorescence microscpoe UCL"

PICTURES_DIR = "pictures"

PROCESSED_FILES_DIR = "processed"

SUPPORTED_FILE_FORMATS = {"JPG", "PNG", "BMP"}


SHOW_IMAGES = False







def main():





	results_file = DATA_PATH + "/results.csv"
	results_fh = open(results_file, "w")
	
	results_fh.write("\t".join(["File", "Blue", "Green", "Ratio"]) + "\n")


	pictures_path = DATA_PATH + "/" + PICTURES_DIR


	# scan Cell Lines dir
	for test_dir in sorted(os.listdir(pictures_path)):

		test_path = pictures_path + "/" + test_dir

		# scan every file for elapsed time
		for rel_file in sorted(os.listdir(test_path)):


			file_path = test_path + "/" + rel_file
			
			if (
				(rel_file[-4:].upper() not in [("." + ff.upper()) for ff in SUPPORTED_FILE_FORMATS])
			or
				(not os.path.isfile(file_path))
			):
				# we're not going through these
				continue

			print("Processing `%s`" % file_path)



			# load
			print("..loading image")
			img = cv2.imread(file_path)

			total_blue = total_green = 0

			c = 0
			for c_y in range(len(img)):
				total_blue += sum(p[0] for p in img[c_y])
				total_green += sum(p[1] for p in img[c_y])
				

			# conversion to fractional
			(total_blue, total_green) = (float(total_blue) / 255.0, float(total_green) / 255.0)
			

			results_fh.write("%s\t%.9f\t%.9f\t%.9f\n" % (
				rel_file, total_blue, total_green, (total_blue / total_green)
			))

			#print(total_green, total_blue)
# 			cv2.imshow(file_path, img)
# 			cv2.moveWindow(winname = file_path, x = 200, y = 200)
# 			cv2.waitKey(2000)
# 			cv2.destroyWindow(file_path)



	results_fh.flush()
	results_fh.close()
				
		
		


if (__name__ == "__main__"):
	sys.exit(main())
	

