#!/usr/bin/python3 -uB


import sys, os, time, scipy, re, numpy, cv2, struct, math

from scipy import ndimage
import matplotlib.pyplot as plt

DATA_PATH = os.environ["HOME"] + "/agata_lab/Scratch Assay"

PICTURES_DIR = "pictures"

PROCESSED_FILES_DIR = "processed"

SUPPORTED_FILE_FORMATS = {"JPG", "PNG", "BMP"}


SHOW_IMAGES = False

USE_BLUR = True
USE_THRESHOLD = True
USE_L2_DETECTOR = False

ARTIFICIAL_EDGE_WIDTH = 8


# This number represents a saturated well (in terms of detected cell count)
FULL_CONFLUENCY_EXPECTATION = 8000


BASE_CELL_MARKER_RADIUS = 12

# If cells count in a given well falls below this fraction of the original count,
# we discard the whole series
MIN_ACCEPTABLE_CELL_SURVIVAL = 1.00



# Gaussian weighting based on vertical position of a given white pixel.
# With the default (1.0 / (math.e * math.pi)) the gauss bell falls to 0 precisely
# at the first and last line of pixels
# Mu is always 0.5 (the center of the image)
GAUSS_WEIGHTING_SIGMA = 1.0 / (math.e * math.pi)


def main():



	cell_detector_params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	cell_detector_params.minThreshold = 8
	#cell_detector_params.minThreshold = 100
	cell_detector_params.maxThreshold = 300


	# Filter by Area.
	cell_detector_params.filterByArea = True
	cell_detector_params.minArea = 30
	cell_detector_params.maxArea = 400


	# Filter by Circularity
	cell_detector_params.filterByCircularity = False
	cell_detector_params.minCircularity = 0.02
	cell_detector_params.maxCircularity = 100000

	# Filter by Convexity
	cell_detector_params.filterByConvexity = False
	cell_detector_params.minConvexity = 0.01
	cell_detector_params.maxConvexity = 100000

	# Filter by Inertia
	cell_detector_params.filterByInertia = False
	cell_detector_params.minInertiaRatio = 0.05
	cell_detector_params.maxInertiaRatio = 100000

	cell_detector = cv2.SimpleBlobDetector_create(cell_detector_params)



	# Setup SimpleBlobDetector parameters.
	patches_detector_params = cv2.SimpleBlobDetector_Params()

	# Change thresholds
	patches_detector_params.minThreshold = 0
	patches_detector_params.maxThreshold = 300


	# Filter by Area.
	patches_detector_params.filterByArea = False
	patches_detector_params.minArea = 1000
	patches_detector_params.maxArea = 0


	# Filter by Circularity
	patches_detector_params.filterByCircularity = False
	patches_detector_params.minCircularity = 0.02
	patches_detector_params.maxCircularity = 100000

	# Filter by Convexity
	patches_detector_params.filterByConvexity = False
	patches_detector_params.minConvexity = 0.01
	patches_detector_params.maxConvexity = 100000

	# Filter by Inertia
	patches_detector_params.filterByInertia = False
	patches_detector_params.minInertiaRatio = 0.05
	patches_detector_params.maxInertiaRatio = 100000


	patches_detector = cv2.SimpleBlobDetector_create(patches_detector_params)



	results_file = DATA_PATH + "/results.csv"
	results_fh = open(results_file, "w")
	
	results_fh.write("\t".join([
		"Line", "Concentration", "Replica", "Passage", "Hours", "Well",
		"Treatment", "CellsDetected", "Confluency", "ScratchConfluency"
	]) + "\n")


	pictures_path = DATA_PATH + "/" + PICTURES_DIR

	# scan Cell Lines dir
	for test_dir in sorted(os.listdir(pictures_path)):

		test_path = pictures_path + "/" + test_dir
		(cell_line, concentration) = test_dir.split("_")

		# scan every cell line for elapsed times
		for rnp_dir in sorted(os.listdir(test_path)):
		
			rnp_path = test_path + "/" + rnp_dir
			
			# remove prefixes and cast to integer
			(replica, passage) = [int(s[1:]) for s in rnp_dir.split("_")]
			
			processed_dir = rnp_path + "/" + PROCESSED_FILES_DIR

			try:
				os.mkdir(processed_dir)
			except FileExistsError:
				pass


			treatment_and_well = disc_radius = tnw_initial_confluency = None


			# scan every file for elapsed time
			for rel_file in sorted(os.listdir(rnp_path)):


				file_path = rnp_path + "/" + rel_file
				if (
					(rel_file[-4:].upper() not in [("." + ff.upper()) for ff in SUPPORTED_FILE_FORMATS])
				or
					(not os.path.isfile(file_path))
				):
					# we're not going through these
					continue


				print("Processing `%s`" % file_path)

				# stages if image processing are held here
				# Takes lots of memory
				p_stages = []


				# note that we also eliminate the extension here
				(treatment, well, hours) = ".".join(rel_file.split(".")[0:-1]).split("_")

				well = int(well)
				hours = int(hours[0:-1])

				# detect change of treatment and/or well
				t_tnw = "%s_%d" % (treatment, well)
				if ((treatment_and_well is None) or (t_tnw != treatment_and_well)):
					# well has changed we reset
					print("Switching to treatment `%s`, well %d" % (treatment, well))
					treatment_and_well = t_tnw
					tnw_initial_confluency = None


	
				# load
				print("..loading image")
				p_stages.append(cv2.imread(file_path))


				res_x = len(p_stages[-1][0])
				res_y = len(p_stages[-1])
				hypotenuse = float((res_x ** 2 + res_y ** 2) ** 0.5)

				print("..detecting cells...")
				detected_cells = cell_detector.detect(p_stages[-1])
				
				

				
				
				if (tnw_initial_confluency is None):

					tnw_initial_confluency = len(detected_cells)

					# thiw will be important next step
					disc_radius = int(round(
						((float(FULL_CONFLUENCY_EXPECTATION) / float(tnw_initial_confluency)) ** 0.5) * BASE_CELL_MARKER_RADIUS
					))
					print("Cell marker disc radius set to %d pixels" % disc_radius)

				print("....%d blobs detected" % len(detected_cells))


				p_stages.append(cv2.drawKeypoints(
					image = p_stages[-1],
					keypoints = detected_cells,
					outImage = numpy.array([]),
					color = (0,0,255),
					flags = cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
				))



				# bubbles canvas!!!

				print("..drawing homogeneus markers...")
				p_stages.append(numpy.zeros(
					shape = (res_y, res_x),
					dtype = numpy.uint8
				))
				for cell in detected_cells:
					cv2.circle(
						img = p_stages[-1],
						center = (int(round(cell.pt[0])), int(round(cell.pt[1]))),
						radius = disc_radius,
						color = (255, 255, 255),
						thickness = cv2.FILLED
					)

				# blur
				#p_stages.append(p_stages[-1].copy())



				print("..closing...")
				close_kernel = numpy.ones(
					shape = (3, 3),
					dtype = numpy.uint8
				)
				p_stages.append(cv2.morphologyEx(
					src = p_stages[-1],
					op = cv2.MORPH_CLOSE,
					kernel = close_kernel,
					iterations = 2
				))


				if (USE_BLUR):
					blur_kernel = numpy.ones(
						shape = (3, 3),
						dtype = numpy.uint8
					)
					print("..blurring...")
					p_stages.append(cv2.blur(
						src = p_stages[-1],
						ksize = (100, 60),
						#sigmaX = 10000,
						#sigmaY = 10000
					))

				if (USE_THRESHOLD):
					print("..thresholding...")
					p_stages.append(cv2.threshold(
						src = p_stages[-1],
						thresh = 127,
						maxval = 255,
						type = cv2.THRESH_BINARY
					)[1])


				if (USE_BLUR and USE_THRESHOLD and USE_L2_DETECTOR):

					print("..adding border...")
					p_stages.append(p_stages[-1].copy())
					p_stages[-1][0:res_y, 0:ARTIFICIAL_EDGE_WIDTH] = numpy.full(
						shape = (res_y, ARTIFICIAL_EDGE_WIDTH),
						fill_value = 255,
						dtype = numpy.uint8
					)
					p_stages[-1][0:res_y, (res_x - ARTIFICIAL_EDGE_WIDTH):res_x] = numpy.full(
						shape = (res_y, ARTIFICIAL_EDGE_WIDTH),
						fill_value = 255,
						dtype = numpy.uint8
					)


					print("..detecting patches...")
					detected_patches = patches_detector.detect(p_stages[-1])

					print("..drawing patch markers...")
					p_stages.append(cv2.drawKeypoints(
						image = p_stages[-1],
						keypoints = detected_patches,
						outImage = numpy.array([]),
						color = (0,0,255),
						flags = cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
					))


					print("..the following patches were detected:")
					for patch in detected_patches:
						print("....%s" % patch)
				else:
					
					print("..weighting...")
					
					# we need color for this stage
					p_stages.append(cv2.cvtColor(
						src = p_stages[-1],
						code = cv2.COLOR_GRAY2BGR
					))

					# calculate the healing score by sampling the pixels

					WEIGHT_LINES_LENGTH = 40

					# rectangle where the saturation "charts" are shown
					cv2.rectangle(
						img = p_stages[-1],
						pt1 = (0, 0),
						pt2 = (WEIGHT_LINES_LENGTH * 3 + 4, res_y - 1),
						color = (40, 40, 40),
						thickness = cv2.FILLED
					)


					confluencies = []
					scratch_confluencies = []
					line_weights = []

					# gauss-based weighting of pixels
					for y_offset in range(res_y):
						
						# here's where we use the gaussian weighting
						line_weight = math.e ** (-(((float(y_offset) / float(res_y)) - 0.5) ** 2.0) / (2.0 * (GAUSS_WEIGHTING_SIGMA ** 2.0)))

						line_weights.append(line_weight)

						# cheap trick
						white_pixels = int(sum(p_stages[-2][y_offset])) / 255
						

						white_pixels_ratio = float(white_pixels) / float(res_x)
						white_pixels_ratio_weighted = white_pixels_ratio * line_weight

						confluencies.append(white_pixels_ratio)
						scratch_confluencies.append(white_pixels_ratio_weighted)
						
						# we draw 3 consecutive lines on the left=hand side:
						# - the gaussian weight of the row (blue)
						# - the ratio of white pixels to black (green)
						# - the above ratio, multiplied by gaussian weighting (red)

						base_offset = 0
						
						for (line_value, line_color) in [
							(line_weight, (255, 0, 0)),
							(white_pixels_ratio, (0, 255, 0)),
							(white_pixels_ratio_weighted, (0, 0, 255))
						]:
							actual_length = int(round(WEIGHT_LINES_LENGTH * line_value))
							cv2.line(
								img = p_stages[-1],
								pt1 = (base_offset, y_offset),
								pt2 = (base_offset + actual_length - 1, y_offset),
								color = line_color
							)


							base_offset += WEIGHT_LINES_LENGTH

					print("AVERAGE WEIGHT: %.9f" % (sum(line_weights) / float(len(line_weights))))
					exit(0)
					confluency = sum(confluencies) / len(confluencies)
					scratch_confluency = sum(scratch_confluencies) / len(scratch_confluencies)


				# stitching

				stitchup = None
				for stage_id in range(len(p_stages)):

					# back to RGB
					if ("ARRAY" not in p_stages[stage_id][0][0].__class__.__name__.upper()):
						p_stages[stage_id] = cv2.cvtColor(
							src = p_stages[stage_id],
							code = cv2.COLOR_GRAY2BGR
						)

					next_tile = p_stages[stage_id]

					if (stitchup is None):
						stitchup = next_tile
					else:
						stitchup = numpy.concatenate(
							(stitchup, next_tile),
							axis = 1
						)

				stages_strip_y = int(round(res_y / len(p_stages)))
				stitchup = cv2.resize(
					src = stitchup,
					#dst = None,
					dsize = (res_x, stages_strip_y),
					interpolation = cv2.INTER_CUBIC
				)

				stitchup = cv2.copyMakeBorder(
					src = stitchup,
					#dst = None,
					top = 0,
					bottom = res_y,
					left = 0,
					right = 0,
					borderType = cv2.BORDER_CONSTANT,
					value = (0, 0, 0)
				)

				# overly the final stage
				stitchup[stages_strip_y:stages_strip_y + res_y, 0:res_x] = p_stages[-1]




				print("..writing output record")
				results_fh.write("\t".join([
					cell_line,
					concentration,
					"%d" % replica,
					"%d" % passage,
					"%d" % hours,
					"%d" % well,
					"%s" % treatment,
					"%d" % len(detected_cells),
					"%.9f" % confluency,
					"%.9f" % scratch_confluency
				]) + "\n")




				out_file = processed_dir + "/" + ".".join(rel_file.split(".")[0:-1] + ["png"])
				print("..saving output in `%s`" % out_file)
				cv2.imwrite(
					filename = out_file,
					img = stitchup,
					params = (cv2.IMWRITE_PNG_COMPRESSION, 9)
				)


				if (SHOW_IMAGES):
					cv2.imshow(file_path, stitchup)
					cv2.moveWindow(winname = file_path, x = 200, y = 200)
					cv2.waitKey(500)
					cv2.destroyWindow(file_path)


	results_fh.close()
				
		
		


if (__name__ == "__main__"):
	sys.exit(main())
	


#img_in = ndimage.imread(os.environ["HOME"] + "/agata_lab/png/18-04-09 SKBR3_SKBR3 P57 24 pl1-3.png")

#plt.imshow(img_in, cmap = plt.cm.gray)
#plt.show()



#return 0


