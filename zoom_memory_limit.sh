#!/bin/bash -eu



declare -r -x PATH='/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin';

declare -i zoom_pid=-1;

declare -r -i LIMIT_MB=1024;
declare -r LIMIT_CG="/sys/fs/cgroup/memory/zoom_sucks";


	set -eu;

	zoom_pids="$(pidof -s zoom || true)";
	
	if [ -z "${zoom_pids}" ]; then
		printf 'Zoom not running\n';
	fi;

	mkdir -pv "${LIMIT_CG}";
	printf '%d' $((1048576 * ${LIMIT_MB})) 1>"${LIMIT_CG}/memory.limit_in_bytes";

	for zoom_pid in ${zoom_pids}; do

		printf '%d\n' ${zoom_pid} 1>"${LIMIT_CG}/cgroup.procs";

	done;
